const fs = require("fs");

function writeTask(fileName, fileContent) {
  return new Promise(function (resolve, reject) {
    fs.writeFile("./files/" + fileName, fileContent, {}, function (err, done) {
      if (err) {
        reject("Error: File was not created", err);
      } else {
        resolve("Success, File has been created", done);
      }
    });
  });
}

module.exports = {
  writeTask,
};

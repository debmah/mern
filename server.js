const http = require("http");
const fileOpWrite = require("./writeTask");
const fileOpRead = require("./readTask");
port = 9090;
const server = http.createServer(function (request, response) {
  //this is callback for handling each client request
  console.log("client connect to server");
  //request or 1st argument is http request object
  // response or 2nd argument is http response object
  console.log("request url>>>", request.url);

  if (request.url === "/write") {
    fileOpWrite
      .writeTask(
        "newFileFromServer.txt",
        "this file was created from server.js"
      )
      .then(function (data) {
        response.end(
          "file was created successful from the server and using port is" + port
        );
      })
      .catch(function (err) {
        response.end("unable to create file from the port " + port);
      });
  } else if (request.url === "/read") {
    fileOpRead
      .readTask("newFileFromServer.txt")
      .then(function (data) {
        console.log("read task successful", data);
        response.end("resoponse end", data);
      })
      .catch(function (err) {
        console.log("Error: read task not succesfull", err);
        response.end("error", err);
      });
  } else {
    response.end("End the response");
  }

  response.end("end of response");
});

//protocol http
// https ===>verb(methods) ===> GET,PUT, POST, DELETE, PATCH, ... AND MANY MORE

server.listen(port, "localhost", function (error) {
  if (error) {
    console.log("server is not listening", error);
  } else {
    console.log("server is listening on port", port);
    console.log("press ctrl+c to exit");
  }
});

const taskToWrite = require("./writeTask");
const taskRead = require("./readTask");

taskToWrite
  .writeTask("newfile.txt", "this is content for newfile.txt")
  .then(function (data) {
    console.log("data/success is >>>", data);
  })
  .catch(function (err) {
    console.log("err is >>>", err);
  });

taskRead
  .readTask("newfile.txt")
  .then(function (data) {
    console.log("data/success to read file >>>", data);
  })
  .catch(function (err) {
    console.log("error to read file >>>>", err);
  });

console.log("dont wait for task");

const fs = require("fs");

function readTask(fileName) {
  return new Promise(function (resolve, reject) {
    fs.readFile("./files/" + fileName, "UTF-8", function (err, done) {
      if (err) {
        reject("Unable to read", err);
      } else {
        resolve("File read success, the content is >>>", done);
      }
    });
  });
}

module.exports = {
  readTask,
};
